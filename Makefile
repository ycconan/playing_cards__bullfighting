#使用g++会报错libboost_system.so: undefined reference to “XXXXXXXXXXXXXXXX”
#一般来说gcc编译c++文件需要加-lstdc++ 链接C++库 但是这里加了反而会报错，或许加了lstdc++引进了一个版本不对应的库，具体原因不明   加-lc表示链接c库

cc = gcc
prom = nn
deps = $(shell find ./include/common/ -name "*.h")
src = $(shell find ./src/ -name "*.cpp")
obj = $(src:%.cpp = %.o)

$(prom): $(obj)
#-D_GLIBCXX_USE_CXX11_ABI=0 #使用hash_map时 添加-std=c++0x 以及 -Wno-deprecated这两个忽略警告选项
	$(cc) -o $(prom) $(obj)  -lstdc++ -Wno-deprecated -lboost_system -D_GLIBCXX_USE_CXX11_ABI=0 #-std=c++11 -std=c++0x

%.o: %.cpp $(deps)
	$(cc) -c $< -o $@

clean:
	rm -rf $(obj) $(prom)	





#gcc ./src/test.cpp ./src/common/*.cpp ./src/common/json/*cpp  -o test -std=c++0x -Wno-deprecated -lboost_system -l/usr/lib64/libstdc++.so.6cd
