
#include "../../include/common/StringConverter.h"
#include <sstream> //使用stringstream需要引入这个头文件
// istringstream类用于执行C++风格的串流的输入操作。
// ostringstream类用于执行C风格的串流的输出操作。
// strstream类同时可以支持C风格的串流的输入输出操作。
// istringstream的构造函数原形如下：
// istringstream::istringstream(string str);
// 它的作用是从string对象str中读取字符。

using namespace std;


//模板函数：将string类型变量转换为常用的数值类型（此方法具有普遍适用性）
template <class Type>
Type StringConverter::stringTo(const String &str)
{
  istringstream iss(str);
  Type wantonly;
  iss >> wantonly;
  return wantonly;
}
double StringConverter::parseDouble(const String &val)
{
    return stringTo<double>(val);
}

/**************未实现*******************/
Real StringConverter::StringConverter::parseReal(const String &val) {}
int StringConverter::StringConverter::parseHex(const String &val) {}
int StringConverter::parseInt(const String &val){}
unsigned int StringConverter::parseUnsignedInt(const String &val) {}
long StringConverter::parseLong(const String &val) {}
unsigned long StringConverter::parseUnsignedLong(const String &val) {}
bool StringConverter::parseBool(const String &val) {}
Vector3 StringConverter::parseVector3(const String &val) {}
StringVector StringConverter::parseStringVector(const String &val) {}
uint64 StringConverter::parseUnsigned64(const String &val) {}
int64 StringConverter::parseInt64(const String &val) {}
time_t StringConverter::parseDateTime(const String &val) {}
String StringConverter::toDateTime(const time_t &val) {}