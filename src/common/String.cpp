
#include <iostream>
#include <vector>
#include "../../include/common/String.h"

using namespace std;

StringUtil::StringUtil(){};

StringUtil::~StringUtil(){};

std::vector<String> StringUtil::split(const String &str, const String &delims, unsigned int maxSplits)
{
    StringUtil *ptr;
    vector<String> res;
    if("" == str) return res;
    //将切割的字符串转换为char*类型
    char *strs = new char[str.length() + 1];
    strcpy(strs , str.c_str());

    char * d = new char[delims.length() + 1];
    strcpy(d, delims.c_str());

    char *p = ptr->strtok(strs, d);//创建实例调用非静态成员函数

    while(p) {

        String s = p; //分割得到的字符串转换为string类型
        res.push_back(s);//存入数组
        p = ptr->strtok(NULL, d);
    }

    return res;
}

char *StringUtil::strtok(char *str, const char *delim)
{
        static char *src=NULL;  //记下上一次非分隔字符串字符的位置,函数体内的局部静态变量只在该函数第一次调用时初始化
        const char *indelim=delim;//对delim做一个备份
        int flag=1,index=0;                                
    //每一次调用strtok,flag标记都会使得程序只记录下第一个非分隔符的位置,以后出现非分隔符不再处理
        char *temp=NULL; //程序的返回值
 
        if(str==NULL) {
          str=src;  //若str为NULL则表示该程序继续处理上一次余下的字符串
        }
        for(;*str;str++)
        {
            delim=indelim;
          for(;*delim;delim++)
            {
                if(*str==*delim)
                    {
                        *str=0; //若找到delim中感兴趣的字符,将该字符置为NULL
                        index=1;   //用来标记已出现感兴趣字符
                        break;
                    }
            }
          if(*str!=0&&flag==1){
                temp=str;         //只记录下当前第一个非感兴趣字符的位置
                flag=0;  
            }
          if(*str!=0&&flag==0&&index==1){
                src=str;          //第二次出现非感兴趣字符的位置(之前一定出现过感兴趣字符)
                return temp;
            }
        }
                src=str;                              
     //执行该句表明一直未出现过感兴趣字符,或者说在出现了感兴趣的字符后,就没再出现过非感兴趣字符
        return temp;
}
