#include <iostream>
#include <vector>
#include <ctime>
#include <map>
#include <string>
#include <string.h>
#include <stdint.h>
#include <algorithm>
#include <fstream>
#include <memory>
#include "../include/common/String.h"
#include "../include/common/json/value.h"
#include "../include/common/json/config.h"
#include "../include/common/json/reader.h"
#include "../include/common/StringConverter.h"

//牌数据
#define SETC_COLPOI(c, p) ((p << 4) + c)
#define GETC_COLOR(v) (v & 0xf)
#define GETC_POINT(v) ((v >> 4) & 0xf)
#define GETC_REALPOINT(v) (((v >> 4) & 0xf) > 10 ? 10 : (v >> 4) & 0xf)

#define MUST 100000000 //比例
#define GET_PRREALVAL(r) (r * MUST)

#define NUM 5
#define TOTAL 52

#define CATTLE "CattleType"
#define _GAME_JSONFILE_PATH_ "./resource"

typedef uint32_t uint32;
typedef uint16_t uint16;
typedef uint8_t uint8;
typedef typename std::vector<uint16 *> cattCob_t;
typedef typename std::map<uint8, cattCob_t> evCattCob_t;
typedef Json::Value JsonValue;

enum enMarchType
{
    enMarchType_FiveSmall = 13,  // 五小牛
    enMarchType_AllTheSame = 12, // 四炸
    enMarchType_FiveFlower = 11, // 五花牛
};
//牌信息数据
enum CardColorDef
{
    CardColor_Lozenge = 1, //方块
    CardColor_Plum = 2,    //梅花
    CardColor_Red = 3,     //红桃
    CardColor_Black = 4,   //黑桃
};

//牛牛牌型
enum CardType
{
    CardType_FiveSmallCattle = 13,
    CardType_AllTheSame = 12,
    CardType_FiveFlowerCattle = 11,
    CardType_CattleCattle = 10,
    CardType_CattleNine = 9,
    CardType_CattleEight = 8,
    CardType_CattleSeven = 7,
    CardType_CattleSix = 6,
    CardType_CattleFive = 5,
    CardType_CattleFour = 4,
    CardType_CattleThree = 3,
    CardType_CattleTwo = 2,
    CardType_CattleOne = 1,
    CardType_NoCattle = 0,

};

struct stBountyInfo
{
    uint8 tId;  //牌型序号
    double pR;  //概率
    float mult; //中奖倍数

    stBountyInfo():tId(0),pR(0),mult(0){}
/*     {
        init();
    }
    void init()
    {
        tId = 0;
        pR = 0;
        mult = 0;
    } */
};

using namespace std;
class OverTwiceJoy
{
  public:
    OverTwiceJoy();

    uint32 RandomUint(uint32 val);

    void InitXml();
    bool ReadJson();

    cattCob_t &GetCardCombin() { return m_CardCombin; }
    uint8 GetCardTypeNum() { return m_nCardTypeNum; }
    //获取牌型序号
    uint8 GetTheCardType();

    //获取某一组牌信息
    uint16 *GetCardTypeInfo();

    //创建牌
    void OnCreateCards();

    //牌组合算法
    void Combine(std::vector<uint16> &data, uint16 n, uint16 m);

    //牌型分类
    bool BrandClassification(std::vector<uint16_t *> &cattcob);

    //五小牛
    bool FiveSmallCattle(uint16_t *cattcob);

    //四炸
    bool AllTheSame(uint16_t *cattcob);

    //五花牛
    bool FiveFlowerCattle(uint16_t *cattcob);

    //牛牛
    bool CattleCattle(uint16_t *cattcob);

    //牛9~1
    bool NineToOneCattle(uint16_t *cattcob);

    //无牛
    bool NoCattle(uint16_t *cattcob);

  private:
    uint8 m_nCardTypeNum;               //牌型序号
    cattCob_t m_CardCombin;             //牌的组合
    evCattCob_t m_EveryCattleGroupInfo; //总集
    std::map<uint8, stBountyInfo> m_mapBountyGroup;
};

OverTwiceJoy::OverTwiceJoy()
{
}

uint32 OverTwiceJoy::RandomUint(uint32 val)
{
    srand((unsigned)time(NULL));
    return rand() % val;
}

void OverTwiceJoy::InitXml()
{

    stBountyInfo bPR;
    //获取牌型相应配置信息
    bPR.tId = 0;
    bPR.pR = 0.00023933;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(0, bPR));
    bPR.tId = 1;
    bPR.pR = 0.0641;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(1, bPR));
    bPR.tId = 2;
    bPR.pR = 0.0647;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(2, bPR));
    bPR.tId = 3;
    bPR.pR = 0.0641;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(3, bPR));
    bPR.tId = 4;
    bPR.pR = 0.0647;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(4, bPR));
    bPR.tId = 5;
    bPR.pR = 0.0641;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(5, bPR));
    bPR.tId = 6;
    bPR.pR = 0.0647;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(6, bPR));
    bPR.tId = 7;
    bPR.pR = 0.0641;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(7, bPR));
    bPR.tId = 8;
    bPR.pR = 0.0647;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(8, bPR));
    bPR.tId = 9;
    bPR.pR = 0.0641;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(9, bPR));
    bPR.tId = 10;
    bPR.pR = 0.0733;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(10, bPR));
    bPR.tId = 11;
    bPR.pR = 0.00030474;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(11, bPR));
    bPR.tId = 12;
    bPR.pR = 0.3458;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(12, bPR));
    bPR.tId = 13;
    bPR.pR = 0.00014775;
    bPR.mult = 1.1;
    m_mapBountyGroup.insert(std::make_pair(13, bPR));

    cout << "OverTwiceJoy::OnCreateCards:"
         << "m_mapBountyGroup size : " << m_mapBountyGroup.size() << endl;
    // std::map<uint8, stBountyInfo>::iterator fit = m_mapBountyGroup.find(bPR.tId);
    // if (fit == m_mapBountyGroup.end())
    // {
    //     m_mapBountyGroup.insert(std::make_pair(bPR.tId, bPR));
    // }
    // else
    // {
    //     fit->second = bPR;
    // }
}

bool OverTwiceJoy::ReadJson()
{
    Json::Reader reader;
    JsonValue N_NGameRule;
    std::ifstream is;
    char buff[100] = {0};
    sprintf(buff, "%s/%s", _GAME_JSONFILE_PATH_, "OverTwiceJoy_Conf.json");
    is.open(buff, std::ios::binary);
    if (is.good() == false)
    {
        cout << "OverTwiceJoy::ReadJson:"
             << "Not Found6 " << buff;
        //Log.Error("OverTwiceJoy::ReadJson", "Not Found %s", buff);
        return false;
    }
    if (reader.parse(is, N_NGameRule) == false) //从字符串中解析出json对象
    {
        cout << "OverTwiceJoy::ReadJson:" << buff << "Faild";
        //Log.Error("OverTwiceJoy::ReadJson", "Load %s  Faild", buff);
        return false;
    }
    if (N_NGameRule.isMember("CatllteType"))
    {
        JsonValue value = N_NGameRule["CatllteType"];
        JsonValue::Members member = value.getMemberNames(); //获取所有的
        JsonValue::Members::iterator iter = member.begin();
        for (iter; iter != member.end(); iter++)
        {
            stBountyInfo bPR;
            //获取牌型相应配置信息
            std::string readstr = *iter;
            bPR.tId = uint8((atoi(readstr.c_str())));
            std::vector<String> vecStringCards = StringUtil::split(value[readstr].asString(), ",");
            for (uint8 k = 0; k < vecStringCards.size(); k++)
            {
                bPR.pR = StringConverter::parseDouble(vecStringCards[0]); //获取其他类的静态成员函数直接通过类名调用
                bPR.mult = StringConverter::parseDouble(vecStringCards[1]);
            }
            cout << "OverTwiceJoy::ReadJson:"
                 << " bPR.tId:" << uint32(bPR.tId) << "   bPR.pR:" << bPR.pR << "      bPR.mult:" << bPR.mult << endl;
            std::map<uint8, stBountyInfo>::iterator fit = m_mapBountyGroup.find(bPR.tId);
            if (fit == m_mapBountyGroup.end())
            {
                m_mapBountyGroup.insert(std::make_pair(bPR.tId, bPR));
            }
            else
            {
                fit->second = bPR;
            }
        }
        cout << "OverTwiceJoy::ReadJson:"
             << "m_mapBountyGroup size : " << m_mapBountyGroup.size() << endl;
        //Log.Debug("TWMJGameLogic::GetTaiType", "read success !");
        return true;
    }
}
//获取牌型序号
uint8 OverTwiceJoy::GetTheCardType()
{
    uint32 offset_num = 0;
    std::vector<uint32> prInterval; //离散区间
    std::map<uint8, stBountyInfo>::iterator fit = m_mapBountyGroup.begin();
    for (fit; fit != m_mapBountyGroup.end(); ++fit)
    {
        offset_num += GET_PRREALVAL((*fit).second.pR);
        //cout << "offset_num_" << uint32((*fit).second.tId)<<": "<< GET_PRREALVAL((*fit).second.pR) << endl;
        prInterval.push_back(offset_num);
    }
    cout << "offset_num: " << offset_num << endl;
    uint32 pr_val = RandomUint(offset_num); //随机值
    //pr_val = 99944462;
    cout << "系统随机值： " << pr_val << endl;
    cout << "prInterval size: " << prInterval.size() << endl;
    // std::vector<uint32>::iterator iter =  prInterval.begin();
    // for(iter; iter != prInterval.end(); ++iter)
    // {
    //     cout <<(*iter)<< "   ";
    // }
    // cout <<endl;

    uint32 first_val = 0;
    for (uint8 i = 0; i < prInterval.size(); ++i)
    { //判断落入区间
        if (pr_val >= first_val && pr_val <= prInterval[i])
        {
            m_nCardTypeNum = i;
            cout << first_val << "\t" << prInterval[i] << endl;
            cout << "m_nCardTypeNum:" << (uint16)m_nCardTypeNum << endl;
            return i;
        }

        first_val = prInterval[i];
    }
}
//获取某一组牌信息
uint16 *OverTwiceJoy::GetCardTypeInfo()
{
    uint8 card_num = GetTheCardType(); //配置概率选出牌型序号
    //cout << "OverTwiceJoy::GetCardTypeInfo:" << "抽出牌型: "<< uint8(card_num)<< endl;
    evCattCob_t::iterator fit = m_EveryCattleGroupInfo.find(card_num);
    uint32 pos = RandomUint((*fit).second.size() - 1);
    cattCob_t::iterator iter = (*fit).second.begin() + pos; //从选中牌型集合中系统随机出一组牌
    return *iter;
}

//-------------------------------------牌型组合、判断及归类-------------------------------------------
//创建牌
void OverTwiceJoy::OnCreateCards()
{
    std::vector<uint16> tempCards;
    for (uint8 j = 1; j < 14; ++j)
    {
        tempCards.push_back(SETC_COLPOI(CardColor_Black, j));
        tempCards.push_back(SETC_COLPOI(CardColor_Red, j));
        tempCards.push_back(SETC_COLPOI(CardColor_Plum, j));
        tempCards.push_back(SETC_COLPOI(CardColor_Lozenge, j));
    }

    cout << "OverTwiceJoy::OnCreateCards:"
         << "tempcards size : " << tempCards.size() << endl;
    //所有组合（2598960种）
    Combine(tempCards, TOTAL, NUM);
    cout << "OverTwiceJoy::OnCreateCards:"
         << "m_CardCombin size : " << m_CardCombin.size() << endl;
}
//牌组合算法
void OverTwiceJoy::Combine(std::vector<uint16> &data, uint16 n, uint16 m)
{
    if (m > n)
        return;

    uint16 *pTable = new uint16[n]; //定义标记buf并将其前m个置1
    memset(pTable, 0, sizeof(uint16_t) * n);
    for (uint16 i = 0; i < m; ++i)
        pTable[i] = 1;

    bool bFind = false;
    do
    {
        uint16 k = 0;
        uint16 *combin = new uint16(5); //存储每组牌（五张）
        for (uint16 i = 0; i < n; i++)  //打印当前组合
        {
            if (pTable[i])
            {
                combin[k] = data[i];
                //cout << data[i] << "\t";
                k += 1;
            }
        }
        m_CardCombin.push_back(combin);
        //cout << endl;

        bFind = false;
        for (uint16 i = 0; i < n - 1; i++)
        {
            if (pTable[i] == 1 && pTable[i + 1] == 0)
            {
                swap(pTable[i], pTable[i + 1]); //调换10为01
                bFind = true;

                if (pTable[0] == 0) //如果第一位为0，则将第i位置之前的1移到最左边，如为1则第i位置之前的1就在最左边，无需移动
                {
                    for (uint16 k = 0, j = 0; k < i; k++) //O(n)复杂度使1在前0在后
                    {
                        if (pTable[k])
                        {
                            swap(pTable[k], pTable[j]);
                            j++;
                        }
                    }
                }

                break;
            }
        }
    } while (bFind);

    delete[] pTable;
}

//牌型分类
bool OverTwiceJoy::BrandClassification(std::vector<uint16 *> &cattcob)
{
    if (!cattcob.size())
        return false;
    cout << "OverTwiceJoy::BrandClassification:"
         << "cattcob size : " << cattcob.size() << endl;
    std::vector<uint16 *>::iterator iter = cattcob.begin();
    for (iter; iter != cattcob.end(); ++iter)
    {

        //返回真代表已经分类
        if (FiveSmallCattle(*iter))
            continue;
        if (AllTheSame(*iter))
            continue;
        if (FiveFlowerCattle(*iter))
            continue;
        if (CattleCattle(*iter))
            continue;
        if (NineToOneCattle(*iter))
            continue;
        NoCattle(*iter);
    }
    for (uint8 i = 0; i < m_EveryCattleGroupInfo.size(); ++i)
    {
        cout << "序号：" << (uint16)i << "    " << m_EveryCattleGroupInfo[i].size() << endl;
    }
    cout << "BrandClassification:"
         << "m_EveryCattleGroupInfo size : " << m_EveryCattleGroupInfo.size() << endl;
    return true;
}

//五小牛
bool OverTwiceJoy::FiveSmallCattle(uint16 *cattcob)
{
    if (cattcob == NULL)
        return false;
    uint8 real_point = 0;
    for (uint16 i = 0; i < NUM; ++i)
    {
        real_point += GETC_REALPOINT(cattcob[i]);
        if (GETC_REALPOINT(cattcob[i]) >= NUM)
            return false;
    }
    if (real_point > 10)
        return false;
    //存入对应牌型集合
    evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_FiveSmallCattle);
    if (iter != m_EveryCattleGroupInfo.end())
        iter->second.push_back(cattcob);
    else
    {
        cattCob_t evCA;
        evCA.push_back(cattcob);
        m_EveryCattleGroupInfo.insert(std::make_pair(CardType_FiveSmallCattle, evCA));
        cout << "OverTwiceJoy::FiveSmallCattle："
             << "CardType_FiveSmallCattle : " << CardType_FiveSmallCattle << endl;
    }
    return true;
}
//四炸
bool OverTwiceJoy::AllTheSame(uint16 *cattcob)
{
    if (cattcob == NULL)
        return false;
    uint8 same_point = 0;
    uint16 point = 0;
    for (uint16 i = 0; i < NUM; ++i)
    {
        if (point != 0)
        {
            if (point == GETC_POINT(cattcob[i]))
                ++same_point;
        }
        point = GETC_POINT(cattcob[i]);
    }
    if (same_point != 3)
        return false;
    else
    {
        //存入对应牌型集合
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_AllTheSame);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_AllTheSame, evCA));
            cout << "OverTwiceJoy::AllTheSame："
                 << "CardType_AllTheSame : " << CardType_AllTheSame << endl;
        }
        return true;
    }
}
//五花牛
bool OverTwiceJoy::FiveFlowerCattle(uint16 *cattcob)
{
    if (cattcob == NULL)
        return false;

    for (uint16 i = 0; i < NUM; ++i)
    {
        if (GETC_POINT(cattcob[i]) <= 10) //不是JQK
            return false;
    }
    //存入对应牌型集合
    evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_FiveFlowerCattle);
    if (iter != m_EveryCattleGroupInfo.end())
        iter->second.push_back(cattcob);
    else
    {
        cattCob_t evCA;
        evCA.push_back(cattcob);
        m_EveryCattleGroupInfo.insert(std::make_pair(CardType_FiveFlowerCattle, evCA));
        cout << "OverTwiceJoy::FiveFlowerCattle："
             << "CardType_FiveFlowerCattle : " << CardType_FiveFlowerCattle << endl;
    }
    return true;
}

//牛牛
bool OverTwiceJoy::CattleCattle(uint16 *cattcob)
{
    if (cattcob == NULL)
        return false;

    uint8 two_point = GETC_REALPOINT(cattcob[3]) + GETC_REALPOINT(cattcob[4]);
    uint8 three_point = 0;
    for (uint16 i = 0; i < 3; ++i)
    {
        three_point += GETC_REALPOINT(cattcob[i]);
    }
    if (three_point % 10 != 0) //前三张牌凑不成牛
        return false;

    if (two_point % 10 != 0)
        return false;
    //存入对应牌型集合
    evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleCattle);
    if (iter != m_EveryCattleGroupInfo.end())
        iter->second.push_back(cattcob);
    else
    {
        cattCob_t evCA;
        evCA.push_back(cattcob);
        m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleCattle, evCA));
        cout << "OverTwiceJoy::CattleCattle："
             << "CardType_CattleCattle : " << CardType_CattleCattle << endl;
    }
    return true;
}

//牛9~1
bool OverTwiceJoy::NineToOneCattle(uint16 *cattcob)
{
    if (cattcob == NULL)
        return false;

    uint8 three_point = 0;
    uint8 two_point = 0;
    bool isBreak = true;
    for (uint16 i = 0; i < NUM && isBreak; ++i)
        for (uint16 j = i + 1; j < NUM && isBreak; ++j)
            for (uint16 k = j + 1; k < NUM && isBreak; ++k)
            {
                two_point = 0;
                if (i != j && i != k && j != k)
                {
                    three_point = GETC_REALPOINT(cattcob[i]) + GETC_REALPOINT(cattcob[j]) + GETC_REALPOINT(cattcob[k]);
                    for (uint16 m = 0; m < NUM; ++m)
                    {
                        if (m != i && m != j && m != k)
                            two_point += GETC_REALPOINT(cattcob[m]);
                    }

                    if (three_point % 10 != 0) //前三张牌凑不成牛
                        continue;

                    isBreak = false;
                }
            }

    two_point %= 10;
    if (isBreak && two_point)
        return false;

    switch (two_point)
    {

    //存入对应牌型集合
    case CardType_CattleNine:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleNine);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleNine, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleNine : " << CardType_CattleNine << endl;
        }
    }
    break;
    case CardType_CattleEight:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleEight);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleEight, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleEight : " << CardType_CattleEight << endl;
        }
    }
    break;
    case CardType_CattleSeven:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleSeven);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleSeven, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleSeven : " << CardType_CattleSeven << endl;
        }
    }
    break;

    case CardType_CattleSix:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleSix);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleSix, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleSix : " << CardType_CattleSix << endl;
        }
    }
    break;

    case CardType_CattleFive:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleFive);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleFive, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleFive : " << CardType_CattleFive << endl;
        }
    }
    break;

    case CardType_CattleFour:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleFour);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleFour, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleFour : " << CardType_CattleFour << endl;
        }
    }
    break;

    case CardType_CattleThree:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleThree);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleThree, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleThree : " << CardType_CattleThree << endl;
        }
    }
    break;

    case CardType_CattleTwo:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleTwo);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleTwo, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleTwo : " << CardType_CattleTwo << endl;
        }
    }
    break;

    case CardType_CattleOne:
    {
        evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_CattleOne);
        if (iter != m_EveryCattleGroupInfo.end())
            iter->second.push_back(cattcob);
        else
        {
            cattCob_t evCA;
            evCA.push_back(cattcob);
            m_EveryCattleGroupInfo.insert(std::make_pair(CardType_CattleOne, evCA));
            cout << "OverTwiceJoy::NineToOneCattle："
                 << "CardType_CattleOne : " << CardType_CattleOne << endl;
        }
    }
    break;
    default:
        break;
    }

    return true;
}

//无牛
bool OverTwiceJoy::NoCattle(uint16 *cattcob)
{
    if (cattcob == NULL)
        return false;

    //存入对应牌型集合
    evCattCob_t::iterator iter = m_EveryCattleGroupInfo.find(CardType_NoCattle);
    if (iter != m_EveryCattleGroupInfo.end())
        iter->second.push_back(cattcob);
    else
    {
        cattCob_t evCA;
        evCA.push_back(cattcob);
        m_EveryCattleGroupInfo.insert(std::make_pair(CardType_NoCattle, evCA));
        cout << "OverTwiceJoy::NoCattle："
             << "CardType_NoCattle : " << CardType_NoCattle << endl;
    }
    return true;
}

int main()
{
    OverTwiceJoy overtiwcejoy;

    cout << "game start" << endl;
    cout << "初始化前：" << time(0) << endl;
    //overtiwcejoy.InitXml();
    overtiwcejoy.ReadJson();
    cout << "初始化完成：" << time(0) << endl;
    overtiwcejoy.OnCreateCards();
    cout << "创建及牌组合完毕：" << time(0) << endl;
    overtiwcejoy.BrandClassification(overtiwcejoy.GetCardCombin());
    cout << "分类完成：" << time(0) << endl;
    uint16 *cardcbin = overtiwcejoy.GetCardTypeInfo();
    cout << "获取牌：" << time(0) << endl;
    for (uint32 i = 0; i < 5; ++i)
    {
        cout << i + 1 << ":   "
             << "color：" << GETC_COLOR(cardcbin[i]) << "point: " << GETC_POINT(cardcbin[i]) << endl;
    }
    cout << "CardType: " << uint16(overtiwcejoy.GetCardTypeNum()) << endl;

    return 0;
}