
REM ==============git commit ===================
@set X=./resource/
@set I1=./include/
@set I2=./src



REM start commit code...
::第一次开启
::@git pull origin master 
::@git pull --rebase origin master

@  git add .gitignore
@  git add .Makefile
@  git add %X%/*.json
@  git add %I1%/*.h
@  git add %I2%/*.cpp
@  git add %I2%/*.h
@  git add %I2%/*.inl

git commit -m "readme " 
::提交变更到本地仓库

git push origin master 
::将变更情况提交到远程git服务器

::git push origin master -f
::强制提交

::error: failed to push some refs to 'git@github.com:Tuesdday/pythoncode.git'
::::提交代码时报错

::Cause of occurrence ：github中的README.md 文件不在本地代码目录中

::Terms of settlement ：git pull --rebase origin master  ::执行这一步。

::如果是撤销所有的已经add的文件:
::git reset HEAD .
::如果是撤销某个文件或文件夹：
::git reset HEAD -filename

:: 当我们需要删除暂存区或分支上的文件, 同时工作区也不需要这个文件了, 可以使用
:: 1 git rm file_path
:: 2 git commit -m 'delete somefile'
:: 3 git push
:: 当我们需要删除暂存区或分支上的文件, 但本地又需要使用, 只是不希望这个文件被版本控制, 可以使用
:: git rm --cached file_path
:: git commit -m 'delete remote somefile'
:: git push
:: git rm -r --cached .

::先将项目clone到本地，修改后再push到码云的项目仓库
::$ git clone https://gitee.com/用户个性地址/HelloGitee.git #将远程仓库克隆到本地
::在克隆过程中，如果仓库是一个私有仓库，将会要求用户输入码云的账号和密码。按照提示输入即可。
::
::当然，用户也可以通过配置本地的git配置信息，执行git config命令预先配置好相关的用户信息，配置执行如下：
::
::$ git config --global user.name "你的名字或昵称"
::$ git config --global user.email "你的邮箱"
::修改代码后，在仓库目录下执行下面命令
::
::$ git add . #将当前目录所有文件添加到git暂存区
::$ git commit -m "my first commit" #提交并备注提交信息
::$ git push origin master #将本地提交推送到远程仓库
::出现密码错误使用
::git config --system --unset credential.helper
::再输入昵称








::!/bin/bash
::Date: 2018-6-28
::Author: ycconan
::Version: 1.1
::Description: svn_commit.h - 提供玖万提交代码服务
::NOTE:如果重构工程目录，需修改参数

::%i% 就像全局变量，定义一次之后，在整个脚本中有效
::%%i 就像局部变量，由 for 在每次递归时动态定义，并且仅在对应的 for 中有效

::路径
:: @set s1="./src/interface/"
:: @set s2="./src/world_scripts/"
:: @set i1="./include/interface/"
:: @set i2="./include/scripts/"
:: @set sc="./scripts_xml/"
::
:: @set TMPFILE=""
:: add文件名后缀
:: @set FILE_SUFFIX=*.cpp,h,xml,json 
:: commit文件名后缀
:: @set FILE_SUFFIX_="*.*" 
::
:: git_status   
:: 判断add状态
::     TMP=(`svn status $TMPFILE|grep ^?|awk '{printf "%s ", $1}'`);
:: 命令状态退出值比较
::     if [ $? -eq 0 ];then
::         svn_add $TMP;
::     fi
:: goto:eof
::
:: :git_add  
::     if[not]"$1" != "" ;then
::         svn add $1;
::     fi
:: goto:eof
::
:: :git_ci
::     svn ci $@ ;
:: goto:eof
::
:: :git_main
::
::     FOR %%i IN (%s1%,%s2%,%i1%,%i2%,%sc%) DO
::     (
::        set CPATH=%%i 
::
::     )
:: 
:: @echo off
:: 设置指定目录，多个目录用英文逗号隔开，如果路径中有空格，请给该路径加上英文双引号
:: set dir=
::
:: 设置指定后缀名，多个后缀用英文逗号隔开，同样有空格的要用英文双引号括起来
:: set ext=
::
:: for %%a in ("%cd%",%dir%) do (
::     pushd "%%~a"
::     for %%b in (%ext%) do (
::         for /f "delims=" %%c in ('dir /b "%%~b"') do echo "%%~c"
::     )
::     popd
:: )
::
::             SI_FILE=$(find $CPATH -regextype posix-extended -regex %FILE_SUFFIX%)
::
::             for TMPFILE in $SI_FILE;do
::             svn_status $TMPFILE;
::             done
::
::        ALL_FILE="$CPATH$FILE_SUFFIX_ -m $3" 
:: 
::        if svn_ci $ALL_FILE; then
::        echo -e \\033[34m "svn commit $CPATH*.* Finish...." \\033[0m
::
::        echo " ">> ./$1.log
::        echo svn commit $CPATH$FILE_SUFFIX_ Finish....  >> ./$1.log
::        echo " ">> ./$1.log
::        fi
::     done
::
:: goto:eof
::
:: 三个参数：项目名、子目录名、说明
:: echo "================ svn commit =================="
:: echo "start commit code..."
:: svn_main $1 $2 $3; 
:: echo "=================== end ======================"
